<?php
include "session.php";
include "header.php";
require_once "core.php";

$userOpt = sprintf("SELECT prava FROM osetrovatel WHERE id_osetrovatele = '%s'", $_SESSION['id']);
$userRes = $db->query($userOpt);
if ( !$userRes) {
	die();
}
else {
	$user = $userRes->fetch_assoc();
	if ($user['prava'] == 1)
		$rights = 'zookeeper';
	else if ($user['prava'] == 2)
		$rights = 'main zookeeper';
	else if ($user['prava'] == 3)
		$rights = 'administrator';
}
?>

<h1>Welcome <?php echo $_SESSION['name'].", ".$rights; ?></h1>
<hr>

<div id="submenu">
	<ul>
		<li><a href="password.php">Change password</a></li>
		<li><a href="info.php">Help</a></li>
	</ul>
	<hr>
</div>