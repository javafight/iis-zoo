<?php
include "users.php";

 if (!hasAuth('admin')) { //Protect parameters injection
  printUnAuth();
  die();
 }

function postUser($log, $psswd, $perm, $name, $surname, $rc, $city, $street, $postcode, $tel, $db) {
	if ($stmt = $db->prepare("INSERT INTO osetrovatel (jmeno, prijmeni, rodne_cislo, mesto, ulice, psc, telefon, prava, login, heslo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
		$stmt->bind_param('sssssssiss', $name, $surname, $rc, $city, $street, $postcode, $tel, $perm, $log, md5($psswd));
		$result = $stmt->execute();
		if (!$result) {
			$stmt->close();
			return false;
		}
		else {
			$stmt->close();
			return true;
		}		
	}
}

function updateUser($log, $psswd, $perm, $name, $surname, $rc, $city, $street, $postcode, $tel, $db, $id) {
	if ($stmt = $db->prepare("UPDATE osetrovatel SET jmeno=?, prijmeni=?, rodne_cislo=?, mesto=?, ulice=?, psc=?, telefon=?, prava=?, login=?, heslo=? WHERE id_osetrovatele=?")) {
		$stmt->bind_param('sssssssissi', $name, $surname, $rc, $city, $street, $postcode, $tel, $perm, $log, $psswd, $id);
		$result = $stmt->execute();
		if (!$result) {
			$stmt->close();
			return false;
		}
		else {
			$stmt->close();
			return true;
		}		
	}
}

function checkUnique($login, $db, $flag) {
	if ($flag)
		return true;

	$query = sprintf("SELECT * FROM osetrovatel WHERE login = '%s'", $db->real_escape_string($login));
	$result = $db->query($query);
	
	if ($result->num_rows >= 1)
		return false;
	else
		return true;
}

function checkBirthNumber($rc) {
	if (strlen($rc) != 10 && strlen($rc) != 9)
		return false;

	$year = (int)substr($rc, 0, 2);
	$month = (int)substr($rc, 2, 2);
	$day = (int)substr($rc, 4, 2);
	
	if (is_numeric(substr($rc, 0, 2)) && is_numeric(substr($rc, 2, 2)) && is_numeric(substr($rc, 4, 2)) && is_numeric(substr($rc, 6))) {
		if (($rc % 11) != 0)
			return false;
		if (strlen($rc) == 9 && strcmp(substr($rc, 6, 3), '000'))
			return false;

		if (($month >= 1 && $month <= 12) || ($month >= 51 && $month <= 62) || ($month >= 21 && $month <= 32) || ($month >= 71 && $month <= 82) && ($day >=1 && $day <= 31)) {
			return true;
		}
	}
	return false;
}

function post($log, $psswd, $perm, $name, $surname, $rc, $city, $street, $postcode, $tel, $db) {
	if ( !postUser($log, $psswd, $perm, $name, $surname, $rc, $city, $street, $postcode, $tel, $db)) {
		printError("Error: Insert failed!");
		return true;
	}
	else {
		printPass("Insert successful!");	
		return false;
	}
}

function update($log, $psswd, $perm, $name, $surname, $rc, $city, $street, $postcode, $tel, $db, $id) {
	if ( !updateUser($log, $psswd, $perm, $name, $surname, $rc, $city, $street, $postcode, $tel, $db, $id)) {
		printError("Error: Update failed!");
		return true;
	}
	else {
		printPass("Update successful!");	
		return false;
	}	
}

if (isset($_GET['edit'])) {
	$edit_id = $_GET['edit'];
	$flag = true;
	$dep = sprintf("SELECT DISTINCT * FROM osetrovatel WHERE id_osetrovatele='%s'", $edit_id);
	$depRes = $db->query($dep);
	if (!$depRes) {
		$depRes = "";
	}
	$dp = $depRes->fetch_assoc();
}
else
	$flag = false;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$log = $_POST["log"];
	
	if (isset($edit_id))
		$psswd = $dp['heslo'];
	else
		$psswd = $_POST["psswd"];

	$perm = $_POST["perm"];
	$name = $_POST["name"];
	$surname = $_POST["surname"];

	if ($log == '' || $psswd == '' || $perm == '' || $name == '' || $surname == '') {
		$err = true;
		printError("Required value(s) not set!");
	}
	else {
		if (strlen($psswd) >= 6) {
			if (checkUnique($log, $db, $flag)) {
				if (isset($_POST["rc"]) && $_POST["rc"] != '') {
					if (checkBirthNumber($_POST["rc"]))
						if (!isset($edit_id))
							$err = post($log, $psswd, $perm, $name, $surname, $_POST["rc"], $_POST["city"], $_POST["street"], $_POST["postcode"], $_POST["tel"], $db);
						else
							$err = update($log, $psswd, $perm, $name, $surname, $_POST["rc"], $_POST["city"], $_POST["street"], $_POST["postcode"], $_POST["tel"], $db, $edit_id);
					else {
						$err = true;
						printError("Error: Invalid birth number!");
					}
				}
				else {
					if (!isset($edit_id))
						$err = post($log, $psswd, $perm, $name, $surname, $_POST["rc"], $_POST["city"], $_POST["street"], $_POST["postcode"], $_POST["tel"], $db);
					else
						$err = update($log, $psswd, $perm, $name, $surname, $_POST["rc"], $_POST["city"], $_POST["street"], $_POST["postcode"], $_POST["tel"], $db, $edit_id);
				}
			}
			else {
				$err = true;
				printError("Error: Login with this name already exists!");
			}
		}
		else {
			$err = true;
			printError("Error: Password must be at least 6 chararacter long!");
		}
	}
}

if (isset($_GET['edit'])) {
	$edit_id = $_GET['edit'];
	$flag = true;
	$dep = sprintf("SELECT DISTINCT * FROM osetrovatel WHERE id_osetrovatele='%s'", $edit_id);
	$depRes = $db->query($dep);
	if (!$depRes) {
		$depRes = "";
	}
	$dp = $depRes->fetch_assoc();
}
else
	$flag = false;
?>

<br>
<div id="addForm">
	<form <?php if (isset($_GET['edit'])) echo "action='user_add.php?edit=".$edit_id."'"; else echo "action='user_add.php'"; ?> method="post">
		<div class="addFormItem">
			<label> Login: <span class="small">Required (must be unique)</span></label>
			<input type="text" name="log" <?php if(isset($_POST['log']) && isset($err)) echo ' value="'.$_POST['log'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['login'].'"'; ?>/>
		</div>
		<?php if (!isset($edit_id))
		echo '<div class="addFormItem">
			<label> Password: <span class="small">Required (length at least 6 characters)</span></label>
			<input type="password" name="psswd"/>
		</div>';?>
		<div class="addFormItem">
			<label> Permission: <span class="small">Required</span></label>
			<select name="perm">
			<?php
				if ($_POST['perm'] == '1' || (isset($edit_id) && $dp['prava'] == '1'))
					echo "<option value='1' selected>Zookeeper</option>";
				else
					echo "<option value='1'>Zookeeper</option>";
				if ($_POST['perm'] == '2' || (isset($edit_id) && $dp['prava'] == '2'))
					echo "<option value='2' selected>Main zookeeper</option>";
				else
					echo "<option value='2'>Main zookeeper</option>";
				if ($_POST['perm'] == '3' || (isset($edit_id) && $dp['prava'] == '3'))
					echo "<option value='3' selected>Administrator</option>";
				else
					echo "<option value='3'>Administrator</option>";
			?>
			</select>
		</div>
		<div class="addFormItem">
			<label> Name: <span class="small">Required</span></label>
			<input type="text" name="name" placeholder="e.g. Antonius" <?php if(isset($_POST['name']) && isset($err)) echo ' value="'.$_POST['name'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['jmeno'].'"'; ?>/>
		</div>
		<div class="addFormItem">
			<label> Surname: <span class="small">Required</span></label>
			<input type="text" name="surname" placeholder="e.g. DeLamancha" <?php if(isset($_POST['surname']) && isset($err)) echo ' value="'.$_POST['surname'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['prijmeni'].'"';?>/>
		</div>
		<div class="addFormItem">
			<label> Birth number: <span class="small">Optional</span></label>
			<input type="text" name="rc" placeholder="format: YYMMDDXXXX" <?php if(isset($_POST['rc']) && isset($err)) echo ' value="'.$_POST['rc'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['rodne_cislo'].'"';?>/>
		</div>
		<div class="addFormItem">
			<label> City: <span class="small">Optional</span></label>
			<input type="text" name="city" placeholder="e.g. Atlantida" <?php if(isset($_POST['city']) && isset($err)) echo ' value="'.$_POST['city'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['mesto'].'"';?>/>
		</div>
		<div class="addFormItem">
			<label> Street: <span class="small">Optional</span></label>
			<input type="text" name="street" placeholder="e.g. Yellow 20" <?php if(isset($_POST['street']) && isset($err)) echo ' value="'.$_POST['street'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['ulice'].'"';?>/>
		</div>
		<div class="addFormItem">
			<label> Postcode: <span class="small">Optional</span></label>
			<input type="text" name="postcode" placeholder="e.g. 66666" <?php if(isset($_POST['postcode']) && isset($err)) echo ' value="'.$_POST['postcode'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['psc'].'"';?>/>
		</div>
		<div class="addFormItem">
			<label> Telephone: <span class="small">Optional</span></label>
			<input type="text" name="tel" placeholder="e.g. +420123456789" <?php if(isset($_POST['tel']) && isset($err)) echo ' value="'.$_POST['tel'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['telefon'].'"';?>/>
		</div>
		<div class="addFormItem">
			<input type="submit" name="submit" <?php if (isset($edit_id)) echo "value='Edit'"; else echo "value='Add'"; ?>>
		</div>
	</form>
</div>

<?php
if (isset($edit_id))
	echo "<div id='submenu'><div id='deparmenu'><a href='user_detail.php?id=".$edit_id."'>[<] Back to user detail</a><br><a href='user_list.php'>[<] Back to user list</a></div></div>";
include "footer.php";
?>