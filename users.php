<?php
include "session.php";
include "header.php";
require_once "core.php";
?>

<h1>Users</h1>
<hr>

<div id="submenu">
	<ul>
		<li><a href="user_list.php">User list</a></li>
		<?php
		if (hasAuth('edit')) {
			echo "<li><a href='user_add.php'>Add new user</a></li>\n";
		}
		?>
	</ul>
	<hr>
</div>