<?php

session_start();

//Redirect to login page and kill yourself
function redir() {
	header("Location: login.php");
	die();
}


if (!isset($_SESSION['activity'])) {
	//Session not exist => login
	redir();
}

if (time() < $_SESSION['activity'] + 20*60) { //timeout in seconds
	//Session OK => set new activity time
	$_SESSION['activity'] = time();
} else {
	//Session expirated by timeout => login
	session_unset();
	session_destroy();
	redir();
}

?>