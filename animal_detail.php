<?php
include "animals.php";

if (isset($_GET['id'])) {
	$id = $_GET['id'];
} elseif (isset($_POST['id'])) {
	$id = $_POST['id'];
} else {
	printError("Invalid link, use menu to continue");
	die();	
}

echo "<form action='animal_detail.php' method='post'>\n";
echo "<input type='hidden' name='id' value='$id'>\n";

//Delete
if (isset($_POST['delete']) && isset($_POST['del_id'])) {
	if (!hasAuth('edit')) { //Protect parameters injection
		$query = "SELECT id_osetrovatele FROM mereni WHERE id_mereni IN (" . implode(", ", $_POST['del_id']) . ")";
		$result = $db->query($query);

		while ($row = $result->fetch_array()) {
			if ($row['id_osetrovatele'] != $_SESSION['id']) {
				printUnAuth();
				die();
			}
		}
	}

	$ids = implode(", ", $_POST['del_id']);
	$query = "DELETE FROM mereni WHERE id_mereni IN ($ids)";
	
	if ($db->query($query) === TRUE) {
		printPass("Selected Measurement(s) successfully deleted");
	} else {
		printError("Error deleting measurements");
	}
}

//List
$query = 	"SELECT jmeno, datum_narozeni, datum_umrti, zivocich.id_oddeleni, nazev, druh, rod, celed, rad, trida " .
			"FROM zivocich, oddeleni, druh " .
			"WHERE zivocich.id_oddeleni = oddeleni.id_oddeleni AND zivocich.id_druhu = druh.id_druhu AND id_zivocicha = $id ";

$result = $db->query($query);
$animal = $result->fetch_assoc();

$query = "SELECT 1 FROM osetrovatel_oddeleni WHERE id_osetrovatele = $_SESSION[id] AND id_oddeleni = $animal[id_oddeleni]";
$result = $db->query($query);

$isMyDepartment = $result->fetch_assoc();

echo "<div class='indent'>\n";

if (hasAuth('edit'))
	echo "<div  class='navig'><a href='animal_add.php?edit=$id'>[+] Edit animal properties</a></div>\n";

echo "<table>\n";
echo "<tr><td>Name:</td><td><b>" . $animal['jmeno'] . "</b></td></tr>\n";
echo "<tr><td>Department:</td><td>" . $animal['nazev'] . ($isMyDepartment? "<i> (Assigned department)</i>": "") . "</td></tr>\n";
echo "<tr><td>Birth date:</td><td>" . date("d.m.Y", strtotime($animal['datum_narozeni'])) . "</td></tr>\n";

if ($animal['datum_umrti'] <> 0)
	echo "<tr><td>Death date:</td><td>" . date("d.m.Y", strtotime($animal['datum_umrti'])) . "</td></tr>\n";

echo "<tr><td>Species:</td><td>" . $animal['rod'] . " " . $animal['druh'] . "</td></tr>\n";
echo "<tr><td>Classification:</td><td>" . $animal['celed'] . ", " . $animal['rad'] . ", " . $animal['trida'] . "</td></tr>\n";

echo "</table>\n";
echo "</div>\n";

//seznam mereni
if (hasAuth('edit') || $isMyDepartment)
	echo "<div  class='navig'><a href='measurement_add.php?id=$id'>[+] Add new measurement</a></div>\n";

echo "<table class='list'>\n";
echo "<tr> <th>Sel.</th> <th>Date</th> <th>Weight</th> <th>Height</th> <th>Length</th> <th>Note</th> <th>Measured by</th> <th>Edit</th> </tr>\n";

$query = 	"SELECT * " .
			"FROM mereni, osetrovatel " .
			"WHERE id_zivocicha = $id AND mereni.id_osetrovatele = osetrovatel.id_osetrovatele " .
			"ORDER BY datum_mereni DESC";

$measure = $db->query($query);

if (mysqli_num_rows($measure)) {
	while ($row = $measure->fetch_array()) {
		$tabRow = "<tr>\n";
		
		if (hasAuth('edit') || $_SESSION['id'] == $row['id_osetrovatele'])
			$tabRow .= "<td>".delete($row['id_mereni'])."</td>";
		else
			$tabRow .= "<td></td>";

		$tabRow .= "<td>" . date("d.m.Y", strtotime($row['datum_mereni'])) . "</td>";

		//Optional values
		$tabRow .= "<td>" . ($row['hmotnost']? $row['hmotnost']." kg": "---") . "</td>";
		$tabRow .= "<td>" . ($row['vyska']? $row['vyska']." cm": "---") . "</td>";
		$tabRow .= "<td>" . ($row['delka']? $row['delka']." cm": "---") . "</td>";
		$tabRow .= "<td>" . ($row['poznamka']? $row['poznamka']: "---") . "</td>";

		$tabRow .= "<td>$row[jmeno] $row[prijmeni]</td>";

		if (hasAuth('edit') || $_SESSION['id'] == $row['id_osetrovatele'])
			$tabRow .= "<td>".edit("measurement_add.php?edit=$row[id_mereni]")."</td>";
		else
			$tabRow .= "<td></td>";
		
		$tabRow .= "\n</tr>\n";

		echo $tabRow;
	}
} else {
	echo "<tr><td colspan='8'>No measurement found</td></tr>\n";
}

echo "</table>\n";

echo "<input type='submit' name='delete' value='Delete selected'>\n";

echo "</form>\n";

echo "<div class='navig' style='margin-top: 20px;'><a href='animal_list.php'>[<] Back to animal list</a></div>\n";
?>


<?php
include "footer.php";
?>