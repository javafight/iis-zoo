<?php
include "animals.php";

if (isset($_POST['department'])) {
	$department = $_POST['department'];
	$search =  $_POST['search'];
	$search_str = $_POST['search_str'];
	$list = $_POST['list'];
} elseif (isset($_POST['h_department'])) {
	$department = $_POST['h_department'];
	$search =  $_POST['h_search'];
	$search_str = $_POST['h_search_str'];
	$list = $_POST['h_list'];
} else {
	$department = "-1";
	$search = "";
	$search_str = "";
	$list = "alive";
}

if (isset($_POST['delete']) && isset($_POST['del_id'])) {
	if (!hasAuth('edit')) { //Protect parameters injection
		printUnAuth();
		die();
	}

	$ids = implode(", ", $_POST['del_id']);
	$query = "DELETE FROM zivocich WHERE id_zivocicha IN ($ids)";
	//TODO integritni omezeni, umoznit mazat kdyz mereni? smazat i s merenimi?
	
	if ($db->query($query) === TRUE) {
		printPass("Selected Animal(s) successfully deleted");
	} else {
		printError("Error deleting animals");
	}
}

echo "<form action='animal_list.php' method='post'>\n";
echo "<div class='indent'>\n";

//Department Filter
echo "<span class='filter'>\n";
echo "Department: <select name='department'>\n";
echo "<option value='-1'>---</option>\n";

$deps = $db->query("SELECT id_oddeleni, nazev FROM oddeleni ORDER BY nazev");

if ($deps) {
	while ($dep = $deps->fetch_array()) {
		echo "<option value='$dep[id_oddeleni]' " . (($dep['id_oddeleni'] == $department)? "selected": "") . ">$dep[nazev]</option>\n";
	}
}

echo "</select>\n";
echo "</span>\n";

//Search Filter
echo "<span class='filter'>\n";
echo "Search: <select name='search'>\n";
echo "<option value ='druh' " . (($search == "druh")? "selected": "") . ">Species</option>\n";
echo "<option value ='rod' " . (($search == "rod")? "selected": "") . ">Genus</option>\n";
echo "<option value ='celed' " . (($search == "celed")? "selected": "") . ">Family</option>\n";
echo "<option value ='rad' " . (($search == "rad")? "selected": "") . ">Order</option>\n";
echo "<option value ='trida' " . (($search == "trida")? "selected": "") . ">Class</option>\n";
echo "</select>\n";

echo "<input type='text' name='search_str' value='$search_str'>\n";
echo "</span>\n";

//List Filter
echo "<span class='filter'>\n";
echo "List: <select name='list'>\n";
echo "<option value ='alive' " . (($list == "alive")? "selected": "") . ">Only alive</option>\n";
echo "<option value ='dead' " . (($list == "dead")? "selected": "") . ">Only dead</option>\n";
echo "<option value ='all' " . (($list == "all")? "selected": "") . ">All</option>\n";
echo "</select>\n";
echo "</span>\n";

//Submit
echo "<input type='submit' name='filter' value='Filter'>\n";
echo "</div>\n";

//Filters State persistantion
echo "<input type='hidden' name='h_department' value='$department'>\n";
echo "<input type='hidden' name='h_search' value='$search'>\n";
echo "<input type='hidden' name='h_search_str' value='$search_str'>\n";
echo "<input type='hidden' name='h_list' value='$list'>\n";

//--------------------------------

echo "<table class='list'>\n";

$heading = "<tr>";
$heading .= hasAuth('edit')? "<th>Sel.</th>": "";
$heading .= "<th>Name</th> <th>Genus</th> <th>Species</th> <th>Family</th> <th>Order</th> <th>Class</th> <th>Department</th>";
$heading .= hasAuth('edit')? "<th>Edit</th>": "";
$heading .= "<th>Det.</th> </tr>\n";

echo $heading;

$query_filter = "";

if ($department != -1)
	$query_filter .= "AND zivocich.id_oddeleni = $department ";

if ($search_str != "")
	$query_filter .= "AND ".$search." LIKE '%".$search_str."%' ";
	
if ($list == 'alive')
	$query_filter .= "AND datum_umrti IS NULL ";
elseif ($list == 'dead')
	$query_filter .= "AND datum_umrti IS NOT NULL ";

$query = 	"SELECT id_zivocicha, jmeno, datum_narozeni, datum_umrti, nazev, druh, rod, celed, rad, trida " .
			"FROM zivocich, oddeleni, druh " .
			"WHERE zivocich.id_oddeleni = oddeleni.id_oddeleni AND zivocich.id_druhu = druh.id_druhu $query_filter" .
			"ORDER BY nazev, rod, jmeno";

$animals = $db->query($query);

if (mysqli_num_rows($animals)) {
	while ($row = $animals->fetch_array()) {
		$id = $row['id_zivocicha'];

		$tabRow = "<tr>\n";
		$tabRow .= hasAuth('edit')? "<td>".delete($id)."</td>": "";
		$tabRow .= "<td>$row[jmeno]</td> <td>$row[rod]</td> <td>$row[druh]</td> <td>$row[celed]</td> <td>$row[rad]</td> <td>$row[trida]</td> <td>$row[nazev]</td>";
		$tabRow .= hasAuth('edit')? "<td>".edit("animal_add.php?edit=$id")."</td>": "";
		$tabRow .= "<td>".detail("animal_detail.php?id=$id")."</td>";
		$tabRow .= "\n</tr>\n";

		echo $tabRow;
	}

	
} else {
	echo "<tr><td colspan='" . (hasAuth('edit')? "10": "8") . "'>No animals found</td></tr>";
}

echo "</table>\n";

echo hasAuth('edit')? "<input type='submit' name='delete' value='Delete selected'>\n": "";


include "footer.php";
?>