<?php
include "session.php";
include "header.php";
require_once "core.php";

function postMeasurement($date, $weight, $height, $length, $id_animal, $id_user, $note, $db) {
	if ($stmt = $db->prepare("INSERT INTO mereni (id_zivocicha, id_osetrovatele, datum_mereni, hmotnost, vyska, delka, poznamka) VALUES (?, ?, ?, ?, ?, ?, ?)")) {
		$stmt->bind_param('iisddds', $id_animal, $id_user, $date, $weight, $height, $length, $note);
		$result = $stmt->execute();
		if (!$result) {
			$stmt->close();
			return false;
		}
		else {
			$stmt->close();
			return true;
		}		
	}
}

function editMeasurement($date, $weight, $height, $length, $id_animal, $id_user, $note, $db, $id) {
	if ($stmt = $db->prepare("UPDATE mereni SET id_zivocicha=?, id_osetrovatele=?, datum_mereni=?, hmotnost=?, vyska=?, delka=?, poznamka=? WHERE id_mereni=?")) {
		$stmt->bind_param('iisdddsi', $id_animal, $id_user, $date, $weight, $height, $length, $note, $id);
		$result = $stmt->execute();
		if (!$result) {
			$stmt->close();
			return false;
		}
		else {
			$stmt->close();
			return true;
		}		
	}
}

function post($date, $weight, $height, $length, $id_animal, $id_user, $note, $db) {
	if ( !postMeasurement($date, $weight, $height, $length, $id_animal, $id_user, $note, $db)) {
		printError("Error: Insert failed!");
		return true;
	}
	else {
		printPass("Insert successful!");
		return false;
	}
}

function update($date, $weight, $height, $length, $id_animal, $id_user, $note, $db, $id) {
	if ( !editMeasurement($date, $weight, $height, $length, $id_animal, $id_user, $note, $db, $id)) {
		printError("Error: Update failed!");
		return true;
	}
	else {
		printPass("Update successful!");
		return false;
	}
}

if (isset($_GET['edit'])) {
	$edit_id = $_GET['edit'];
	$dep = sprintf("SELECT DISTINCT * FROM mereni WHERE id_mereni='%s'", $edit_id);
	$depRes = $db->query($dep);
	if (!$depRes) {
		$depRes = "";
	}
	$dp = $depRes->fetch_assoc();
	$id_animal = $dp['id_zivocicha'];
}
else if (isset($_GET['id']))
	$id_animal = $_GET['id'];
else {
	printUnAuth();
	die();
}

$id_user = $_SESSION['id'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$date = $_POST["date"];
	$weight = $_POST["weight"];
	$height = $_POST["height"];
	$length = $_POST["length"];
	if ($date == '') {
		$err = true;
		printError("Required value(s) not set!");
	}
	else {
		if ($weight != '') {
			if (is_numeric($weight)) {
				if (!isset($edit_id))
					$err = post($date, $weight, $height, $length, $id_animal, $id_user, $_POST['note'], $db);
				else
					$err = update($date, $weight, $height, $length, $id_animal, $id_user, $_POST['note'], $db, $edit_id);
			}
			else {
				$err = true;
				printError("Error: Weight must be number!");
			}

		}
		else if ($height != '') {
			if (is_numeric($height)) {
				if (!isset($edit_id))
					$err = post($date, $weight, $height, $length, $id_animal, $id_user, $_POST['note'], $db);
				else
					$err = update($date, $weight, $height, $length, $id_animal, $id_user, $_POST['note'], $db, $edit_id);
			}
			else {
				$err = true;
				printError("Error: Height must be number!");
			}

		}
		else if ($length != '') {
			if (is_numeric($length)) {
				if (!isset($edit_id))
					$err = post($date, $weight, $height, $length, $id_animal, $id_user, $_POST['note'], $db);
				else
					$err = update($date, $weight, $height, $length, $id_animal, $id_user, $_POST['note'], $db, $edit_id);
			}
			else {
				$err = true;
				printError("Error: Length must be number!");
			}

		}
		else {
			if (!isset($edit_id))
				$err = post($date, $weight, $height, $length, $id_animal, $id_user, $_POST['note'], $db);
			else
				$err = update($date, $weight, $height, $length, $id_animal, $id_user, $_POST['note'], $db, $edit_id);
		}
	}
}

if (isset($_GET['edit'])) {
	$edit_id = $_GET['edit'];
	$dep = sprintf("SELECT DISTINCT * FROM mereni WHERE id_mereni='%s'", $edit_id);
	$depRes = $db->query($dep);
	if (!$depRes) {
		$depRes = "";
	}
	$dp = $depRes->fetch_assoc();
	$id_animal = $dp['id_zivocicha'];
}
else if (isset($_GET['id']))
	$id_animal = $_GET['id'];
else {
	printUnAuth();
	die();
}
?>
<h1>Add measurement</h1>
<hr>

<br>
<div id="addForm">
	<form <?php if (isset($edit_id)) echo "action='measurement_add.php?edit=".$edit_id."'"; else echo "action='measurement_add.php?id=".$id_animal."'";?> method="post">
		<div class="addFormItem">
			<label> Date: <span class="small">Required</span></label>
			<input type="date" name="date" placeholder='format: YYYY-mm-dd' <?php if(isset($_POST['date']) && $err == true) echo ' value="'.$_POST['date'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['datum_mereni'].'"'; else echo 'value="'.date("Y-m-d").'"';?>/>
		</div>
		<div class="addFormItem">
			<label> Weight: <span class="small">Optional</span></label>
			<input type="text" name="weight" placeholder="kg" <?php if(isset($_POST['weight']) && isset($err)) echo ' value="'.$_POST['weight'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['hmotnost'].'"';?>/>
		</div>
		<div class="addFormItem">
			<label> Height: <span class="small">Optional</span></label>
			<input type="text" name="height" placeholder="cm" <?php if(isset($_POST['height']) && isset($err)) echo ' value="'.$_POST['height'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['vyska'].'"';?>/>
		</div>
		<div class="addFormItem">
			<label> Length: <span class="small">Optional</span></label>
			<input type="text" name="length" placeholder="cm" <?php if(isset($_POST['length']) && isset($err)) echo ' value="'.$_POST['length'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['delka'].'"';?>/>
		</div>
		<div class="addFormItem">
			<label> Note: <span class="small">Optional</span></label>
			<textarea name="note"><?php if(isset($_POST['note']) && isset($err)) echo $_POST['note']; else if (isset($edit_id)) echo $dp['poznamka'];?></textarea>
		</div>
		<div class="addFormItem">
			<input type="submit" name="submit" <?php if (isset($edit_id)) echo "value='Edit'"; else echo "value='Add'"; ?>>
		</div>
	</form>
</div>
<div id="submenu">
	<div id='deparmenu'>
		<?php echo "<a href='animal_detail.php?id=".$id_animal."'>[<] Back to animal detail</a>"; ?>
	</div>
</div>


<?php
include "footer.php";
?>