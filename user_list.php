<?php
include "users.php";

$query =	"SELECT id_osetrovatele, jmeno, prijmeni, telefon, prava, login " .
			"FROM osetrovatel " .
			"ORDER BY prijmeni, jmeno, login";

$result = $db->query($query);

echo "<table class='list'>\n";
echo "<tr> <th>Name</th> <th>Login</th> <th>Phone number</th> <th>Permission</th> " . (hasAuth('admin')? "<th>Edit</th>": "") . "<th>Det.</th> </tr>\n";

if (mysqli_num_rows($result)) {
	while ($user = $result->fetch_array()) {
		$row = "<tr>";
		$row .= "<td>$user[jmeno] $user[prijmeni]</td>";
		$row .= "<td>$user[login]</td>";
		$row .= "<td>$user[telefon]</td>";
		if ($user['prava'] == 1)
			$row .= "<td>Zookeeper</td>";
		elseif ($user['prava'] == 2)
			$row .= "<td>MainZookeper</td>";
		elseif ($user['prava'] == 3)
			$row .= "<td>Administrator</td>";
		else
			$row .= "<td>???</td>";

		if (hasAuth('admin'))
			$row .= "<td>".edit("user_add.php?edit=$user[id_osetrovatele]")."</td>";

		$row.= "<td>".detail("user_detail.php?id=$user[id_osetrovatele]")."</td>";

		$row .= "</tr>\n";

		echo $row;
	}
} else {
	//echo "<tr";
}

include "footer.php";
?>