<?php header('Content-Type: text/html; charset=utf-8'); ?>

<!DOCTYPE HTML>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="style.css">
	<title>ZOO IS</title>
</head>

<body>

<div id="header">
	<div id="left">
		ZOO Information system
	</div>

	<div id="right">
		<?php
			if (isset($_SESSION['name'])) {
				echo $_SESSION['name'] . "<br />" . "<a href='logout.php'>Log out</a>";
			}
		?>
	</div>
</div>

<div id="menu">
	<ul>
		<li><a href="info.php">Home</a></li>
		<li><a href="animal_list.php">Animals</a></li>
		<li><a href="department_list.php">Departments</a></li>
		<li><a href="user_list.php">Users</a></li>
	</ul>
</div>

<div id="content">
