<?php
include "home.php";

function updatePass($psswd, $db, $id) {
	if ($stmt = $db->prepare("UPDATE osetrovatel SET heslo=? WHERE id_osetrovatele=?")) {
		$stmt->bind_param('si', md5($psswd), $id);
		$result = $stmt->execute();
		if (!$result) {
			$stmt->close();
			return false;
		}
		else {
			$stmt->close();
			return true;
		}		
	}
}

$userOpt = sprintf("SELECT heslo FROM osetrovatel WHERE id_osetrovatele = '%s'", $_SESSION['id']);
$userRes = $db->query($userOpt);
$user = $userRes->fetch_assoc();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$pass = $_POST['oldpsswd'];
	$pass1 = $_POST['psswd'];
	$pass2 = $_POST['psswd2'];

	if ($pass1 == '' || $pass2 == '' || $pass == '')
		printError("Required value(s) not set!");
	else {
		if (strlen($pass1) >= 6) {
			if (strcmp($pass1, $pass2) != 0)
				printError("Error: New passwords do not match!");
			else {
				if (md5($pass) != $user['heslo'])
					printError("Error: Wrong old password!");
				else {
					if (!updatePass($pass1, $db, $_SESSION['id']))
						printError("Error: Update failed!");
					else
						printPass("Update successful");
				}
			}
		}
		else
			printError("Error: New password must be at least 6 character long!");
	}
}
?>

<br>
<div id="addForm">
	<form action='password.php' method='post'>
		<div class="addFormItem">
			<label> Old password: <span class="small">Required</span></label>
			<input type="password" name="oldpsswd" />
		</div>
		<div class="addFormItem">
			<label> New password: <span class="small">Required (length at least 6 characters)</span></label>
			<input type="password" name="psswd" />
		</div>
		<div class="addFormItem">
			<label> New password again: <span class="small">Required</span></label>
			<input type="password" name="psswd2" />
		</div>
		<div class="addFormItem">
			<input type="submit" name="submit" value='Change'>
		</div>
	</form>
</div>

<?php
include "footer.php";
?>

