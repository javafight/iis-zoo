-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Počítač: localhost
-- Vygenerováno: Stř 02. pro 2015, 21:51
-- Verze MySQL: 5.6.27
-- Verze PHP: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `xmencn00`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `druh`
--

CREATE TABLE IF NOT EXISTS `druh` (
  `id_druhu` int(11) NOT NULL AUTO_INCREMENT,
  `druh` varchar(100) NOT NULL,
  `rod` varchar(100) NOT NULL,
  `celed` varchar(100) NOT NULL,
  `rad` varchar(100) NOT NULL,
  `trida` varchar(100) NOT NULL,
  PRIMARY KEY (`id_druhu`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

--
-- Vypisuji data pro tabulku `druh`
--

INSERT INTO `druh` (`id_druhu`, `druh`, `rod`, `celed`, `rad`, `trida`) VALUES
(29, 'Pruhohřbetý', 'Kočkodan', 'Kočkodanovití', 'Primáti', 'Savci'),
(30, 'Bezoárová', 'Koza', 'Turovití', 'Sudokopytníci', 'Savci'),
(31, 'Ararauna', 'Ara', 'Papouškovití', 'Papoušci', 'Ptáci'),
(32, 'Armatus', 'Stegosaurus', 'Stegosauridae', 'Ptakopánví', 'Plazi'),
(33, 'Hnědý', 'Medvěd', 'Medvědovití', 'Šelmy', 'Savci'),
(35, 'Zorgan', 'Zhulu', 'Zhuluovci', 'Plazmovzdušní', 'Nehmotní'),
(36, 'Obecný', 'Vlk', 'Psovití', 'Šelmy', 'Savci'),
(37, 'Divoká', 'Kočka', 'Kočkovití', 'Šelmy', 'Savci'),
(38, 'Sappiens', 'Homo', 'Hominidi', 'Primáti', 'Savci');

-- --------------------------------------------------------

--
-- Struktura tabulky `mereni`
--

CREATE TABLE IF NOT EXISTS `mereni` (
  `id_mereni` int(11) NOT NULL AUTO_INCREMENT,
  `id_zivocicha` int(11) NOT NULL,
  `id_osetrovatele` int(11) NOT NULL,
  `datum_mereni` date NOT NULL,
  `hmotnost` double DEFAULT NULL,
  `vyska` double DEFAULT NULL,
  `delka` double DEFAULT NULL,
  `poznamka` mediumtext,
  PRIMARY KEY (`id_mereni`),
  KEY `id_zivocicha` (`id_zivocicha`),
  KEY `id_osetrovatele` (`id_osetrovatele`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Vypisuji data pro tabulku `mereni`
--

INSERT INTO `mereni` (`id_mereni`, `id_zivocicha`, `id_osetrovatele`, `datum_mereni`, `hmotnost`, `vyska`, `delka`, `poznamka`) VALUES
(12, 20, 1, '2015-11-30', 40, 85, 55, ''),
(19, 20, 1, '2015-11-28', 0, 0, 0, 'Zdravotní stav dobrý'),
(20, 20, 1, '2015-10-10', 38, 84, 54, ''),
(21, 7, 1, '2015-12-17', 0, 0, 0, ''),
(23, 15, 1, '2015-12-02', 12, 50, 62, ''),
(26, 26, 15, '2015-12-02', 2.3, 35, 0, ''),
(27, 26, 15, '2015-11-01', 0, 0, 0, 'Somethin very important'),
(28, 26, 15, '2015-12-02', 0, 0, 0, ''),
(29, 26, 15, '2015-06-17', 2, 25, 0, ''),
(30, 28, 15, '2015-10-07', 2, 23, 0, 'Note'),
(31, 28, 15, '2015-11-30', 0, 0, 0, 'Only note no values'),
(33, 28, 15, '2015-06-10', 1.5, 22, 0, ''),
(34, 20, 15, '2015-05-02', 35, 80, 50, ''),
(35, 20, 15, '2015-11-02', 0, 0, 0, 'Prohlídka ok'),
(36, 26, 14, '2015-04-10', 1, 20, 0, ''),
(37, 28, 14, '2015-08-15', 1.8, 20, 0, 'Lorem ipsum'),
(38, 30, 14, '2014-12-02', 55, 55, 44, ''),
(39, 16, 14, '2015-11-02', 1, 2, 3, ''),
(40, 16, 14, '2015-10-02', 1, 2, 2, ''),
(41, 15, 14, '2015-11-15', 10, 20, 30, '');

-- --------------------------------------------------------

--
-- Struktura tabulky `oddeleni`
--

CREATE TABLE IF NOT EXISTS `oddeleni` (
  `id_oddeleni` int(11) NOT NULL AUTO_INCREMENT,
  `typ_umisteni` varchar(100) NOT NULL,
  `nazev` varchar(100) NOT NULL,
  PRIMARY KEY (`id_oddeleni`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Vypisuji data pro tabulku `oddeleni`
--

INSERT INTO `oddeleni` (`id_oddeleni`, `typ_umisteni`, `nazev`) VALUES
(1, 'statek', 'Dětská ZOO'),
(2, 'výběh', 'Júrský svět'),
(3, 'pavilon', 'Pavilon opic'),
(13, 'sladkovodní akvárium', 'Vodní svět'),
(15, 'pláň', 'Sudokopytníci'),
(19, 'savana', 'Africké safari');

-- --------------------------------------------------------

--
-- Struktura tabulky `osetrovatel`
--

CREATE TABLE IF NOT EXISTS `osetrovatel` (
  `id_osetrovatele` int(11) NOT NULL AUTO_INCREMENT,
  `jmeno` varchar(100) NOT NULL,
  `prijmeni` varchar(100) NOT NULL,
  `rodne_cislo` varchar(10) NOT NULL,
  `mesto` varchar(100) DEFAULT NULL,
  `ulice` varchar(100) DEFAULT NULL,
  `psc` varchar(10) DEFAULT NULL,
  `telefon` varchar(20) DEFAULT NULL,
  `prava` int(11) NOT NULL DEFAULT '0',
  `login` varchar(100) NOT NULL,
  `heslo` varchar(500) NOT NULL,
  PRIMARY KEY (`id_osetrovatele`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Vypisuji data pro tabulku `osetrovatel`
--

INSERT INTO `osetrovatel` (`id_osetrovatele`, `jmeno`, `prijmeni`, `rodne_cislo`, `mesto`, `ulice`, `psc`, `telefon`, `prava`, `login`, `heslo`) VALUES
(1, 'Pavel', 'Mencner', '8911283117', 'Javorník', 'Dittersova 547', '79070', '+420728528371', 3, 'mencner', '955db0b81ef1989b4a4dfeae8061a9a6'),
(2, 'Miloš', 'Dolinský', '9456169910', 'Vidnava', 'Fojtství 97', '79055', '+420731266448', 3, 'dolinsky', '5eeffe9e1d75c13683a1abe84eb5a9b5'),
(3, 'Pavel', 'Holub', '9704087734', 'Tišnova', 'Brutální 95', '608 15', '+420722556644', 1, 'holub', 'e10adc3949ba59abbe56e057f20f883e'),
(11, 'Jan', 'Schiller', '8805064818', '', '', '', '', 1, 'schiller', 'e10adc3949ba59abbe56e057f20f883e'),
(12, 'Jiří', 'Jiráček', '', '', '', '', '', 1, 'jiracek', 'e10adc3949ba59abbe56e057f20f883e'),
(13, 'Petr', 'Dostál', '8709044586', 'Brno', 'Milady Horákové 859/6', '602 00', '+420720123456', 3, 'admin', 'e10adc3949ba59abbe56e057f20f883e'),
(14, 'Vladimír', 'Strašák', '9405173063', 'Brno', 'Husova 534/20', '602 00', '+420728444555', 2, 'mainzookeeper', 'e10adc3949ba59abbe56e057f20f883e'),
(15, 'Jiří', 'Novák', '8801076691', 'Brno', 'Koliště 643/23', '602 00', '+420777952647', 1, 'zookeeper', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Struktura tabulky `osetrovatel_oddeleni`
--

CREATE TABLE IF NOT EXISTS `osetrovatel_oddeleni` (
  `id_oddeleni` int(11) NOT NULL,
  `id_osetrovatele` int(11) NOT NULL,
  PRIMARY KEY (`id_oddeleni`,`id_osetrovatele`),
  KEY `id_oddeleni` (`id_oddeleni`),
  KEY `id_osetrovatele` (`id_osetrovatele`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `osetrovatel_oddeleni`
--

INSERT INTO `osetrovatel_oddeleni` (`id_oddeleni`, `id_osetrovatele`) VALUES
(1, 15),
(3, 1),
(3, 2),
(13, 3),
(13, 11),
(15, 3),
(15, 15),
(19, 14),
(19, 15);

-- --------------------------------------------------------

--
-- Struktura tabulky `samice`
--

CREATE TABLE IF NOT EXISTS `samice` (
  `id_zivocicha` int(11) NOT NULL,
  `pocet_vrhu` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `id_zivocicha` (`id_zivocicha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `samice`
--

INSERT INTO `samice` (`id_zivocicha`, `pocet_vrhu`) VALUES
(4, 0),
(7, 0),
(8, 5),
(15, 0),
(19, 5),
(26, 4);

-- --------------------------------------------------------

--
-- Struktura tabulky `zivocich`
--

CREATE TABLE IF NOT EXISTS `zivocich` (
  `id_zivocicha` int(11) NOT NULL AUTO_INCREMENT,
  `id_druhu` int(11) NOT NULL,
  `id_oddeleni` int(11) NOT NULL,
  `jmeno` varchar(100) DEFAULT NULL,
  `datum_narozeni` date DEFAULT NULL,
  `datum_umrti` date DEFAULT NULL,
  PRIMARY KEY (`id_zivocicha`),
  KEY `id_druhu` (`id_druhu`),
  KEY `id_oddeleni` (`id_oddeleni`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Vypisuji data pro tabulku `zivocich`
--

INSERT INTO `zivocich` (`id_zivocicha`, `id_druhu`, `id_oddeleni`, `jmeno`, `datum_narozeni`, `datum_umrti`) VALUES
(4, 30, 15, 'Líza', '2001-07-11', NULL),
(5, 29, 1, 'JoshRobertson', '2015-11-11', NULL),
(7, 29, 3, 'Esmeralda', '2015-10-28', NULL),
(8, 32, 2, 'KarelHermafrodit', '2015-10-29', NULL),
(15, 36, 2, 'Nyxa', '2014-11-03', NULL),
(16, 37, 19, 'Bongo The King', '2012-06-30', NULL),
(18, 37, 19, 'Armando', '2015-11-04', NULL),
(19, 30, 15, 'Chulio Esperanto', '2008-05-10', NULL),
(20, 38, 1, 'Abdul', '2014-08-22', NULL),
(23, 37, 19, 'Pixie', '2005-11-04', '2015-10-10'),
(26, 31, 19, 'Jimmy', '2015-09-10', NULL),
(28, 31, 19, 'Jiří', '2015-12-01', NULL),
(30, 32, 2, 'Steven', '2009-05-01', '2014-06-05');

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `mereni`
--
ALTER TABLE `mereni`
  ADD CONSTRAINT `Mereni_ibfk_1` FOREIGN KEY (`id_zivocicha`) REFERENCES `zivocich` (`id_zivocicha`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Mereni_ibfk_2` FOREIGN KEY (`id_osetrovatele`) REFERENCES `osetrovatel` (`id_osetrovatele`);

--
-- Omezení pro tabulku `osetrovatel_oddeleni`
--
ALTER TABLE `osetrovatel_oddeleni`
  ADD CONSTRAINT `Osetrovatel_oddeleni_ibfk_1` FOREIGN KEY (`id_oddeleni`) REFERENCES `oddeleni` (`id_oddeleni`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Osetrovatel_oddeleni_ibfk_2` FOREIGN KEY (`id_osetrovatele`) REFERENCES `osetrovatel` (`id_osetrovatele`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `samice`
--
ALTER TABLE `samice`
  ADD CONSTRAINT `Samice_ibfk_1` FOREIGN KEY (`id_zivocicha`) REFERENCES `zivocich` (`id_zivocicha`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `zivocich`
--
ALTER TABLE `zivocich`
  ADD CONSTRAINT `zivocich_ibfk_1` FOREIGN KEY (`id_druhu`) REFERENCES `druh` (`id_druhu`),
  ADD CONSTRAINT `zivocich_ibfk_2` FOREIGN KEY (`id_oddeleni`) REFERENCES `oddeleni` (`id_oddeleni`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
