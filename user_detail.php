<?php
include "users.php";

if (isset($_GET['id'])) {
	$id = $_GET['id'];
} elseif (isset($_POST['id'])) {
	$id = $_POST['id'];
} else {
	printError("Invalid link, use menu");
	die();
}

//Canceling assignments on departments
if (isset($_POST['delete']) && isset($_POST['del_id'])) {
	if (!hasAuth('edit')) { //Protect parameters injection
		printUnAuth();
		die();
	}

	$error = FALSE;

	foreach ($_POST['del_id'] as $key => $value) {

		$query = "DELETE FROM osetrovatel_oddeleni WHERE id_oddeleni = $value AND id_osetrovatele = $id";

		if ($db->query($query) === FALSE)
			$error = TRUE;

	}
	
	if (!$error) {
		printPass("Selected assignment(s) successfully canceled");
	} else {
		printError("Error canceling assignment(s)");
	}

}

//User properties
$query =	"SELECT id_osetrovatele, jmeno, prijmeni, rodne_cislo, mesto, ulice, psc, telefon, prava, login " .
			"FROM osetrovatel " .
			"WHERE id_osetrovatele = $id";

$result = $db->query($query);
$user = $result->fetch_assoc();

echo "<form action='user_detail.php' method='post'>\n";
echo "<input type='hidden' name='id' value='$id'>\n";

if ($user) {

	if (hasAuth('admin')) //Editovat admin nebo sam sebe
		echo "<div  class='navig'><a href='user_add.php?edit=$user[id_osetrovatele]'>[+] Edit user properties</a></div>\n";
	
	echo "<table class='indent'>\n";

	echo "<tr> <td>Name:</td> <td><b>$user[jmeno] $user[prijmeni]</b></td> </tr>\n";
	echo "<tr> <td>Login:</td> <td>$user[login]</td> </tr>\n";
	echo "<tr> <td>Birth number:</td> <td>".($user['rodne_cislo']? $user['rodne_cislo']: "---")."</td> </tr>\n";
	echo "<tr> <td>City:</td> <td>".($user['mesto']? $user['mesto']: "---")."</td> </tr>\n";
	echo "<tr> <td>Street</td> <td>".($user['ulice']? $user['ulice']: "---")."</td> </tr>\n";
	echo "<tr> <td>Post code:</td> <td>".($user['psc']? $user['psc']: "---")."</td> </tr>\n";
	echo "<tr> <td>Phone:</td> <td>".($user['telefon']? $user['telefon']: "---")."</td> </tr>\n";
	echo "<tr> <td>Permission:</td>";
	if ($user['prava'] == 1)
			echo "<td>Zookeeper</td>";
		elseif ($user['prava'] == 2)
			echo "<td>MainZookeper</td>";
		elseif ($user['prava'] == 3)
			echo "<td>Administrator</td>";
		else
			echo "<td>???</td>";

	echo "</tr>\n";
	
	echo "</table>\n";

}


//List departments where user is assigned
$query =	"SELECT o.id_oddeleni, nazev, typ_umisteni " .
			"FROM oddeleni o, osetrovatel_oddeleni od " .
			"WHERE o.id_oddeleni = od.id_oddeleni AND od.id_osetrovatele = $id " .
			"ORDER BY nazev";

$result = $db->query($query);

echo "<h2>Assigned departments</h2>\n";
echo "<table class='list'>\n";
echo "<tr>".(hasAuth('edit')? "<th>Sel.</th>": "")."<th>Name</th> <th>Type</th> </tr>\n";

if (mysqli_num_rows($result)) {
	while ($dep = $result->fetch_array()) {
		echo "<tr>";

		if (hasAuth('edit'))
			echo "<td>".delete($dep['id_oddeleni'])."</td>";

		echo "<td>$dep[nazev]</td> <td>$dep[typ_umisteni]</td> </tr>\n";
	}
} else {
	echo "<tr><td colspan='" . (hasAuth('edit')? "3": "2") . "'>No assigned departments</td></tr>\n";
}

echo "</table>\n";
echo hasAuth('edit')? "<input type='submit' name='delete' value='Cancel assignment'>\n": "";
echo "</form>\n";

echo "<div class='navig' style='margin-top: 20px;'><a href='user_list.php'>[<] Back to user list</a></div>\n";

include "footer.php";
?>