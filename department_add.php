<?php
include "departments.php";

 if (!hasAuth('admin')) { //Protect parameters injection
  printUnAuth();
  die();
 }

function postDep($type, $name, $id_user, $db) {
	if ($stmt = $db->prepare("INSERT INTO oddeleni (typ_umisteni, nazev) VALUES (?, ?)")) {
		$stmt->bind_param('ss', $type, $name);
		$result = $stmt->execute();
		if (!$result) {
			$stmt->close();
			return false;
		}
		else {
			$stmt->close();
			$id_dep = $db->insert_id;
			if ($stmt = $db->prepare("INSERT INTO osetrovatel_oddeleni (id_oddeleni, id_osetrovatele) VALUES (?, ?)")) {
				$stmt->bind_param('ii', $id_dep, $id_user);
				$result = $stmt->execute();
				if (!$result) {
					$stmt->close();
					//delete
					$query = sprintf("DELETE FROM oddeleni WHERE id_oddeleni = '%s'", $id_dep);
					$db->query($query);
					return false;
				}
				else {
					$stmt->close();
					return true;
				}
			}
			return true;
		}		
	}
}

function editDep($type, $name, $id_user, $db, $id) {
	if ($stmt = $db->prepare("UPDATE oddeleni SET typ_umisteni=?, nazev=? WHERE id_oddeleni=?")) {
		$stmt->bind_param('ssd', $type, $name, $id);
		$result = $stmt->execute();
		if (!$result) {
			$stmt->close();
			return false;
		}
		else {
			$stmt->close();
			//$id_dep = $db->insert_id;
			if ($stmt = $db->prepare("UPDATE osetrovatel_oddeleni SET id_oddeleni=?, id_osetrovatele=? WHERE id_oddeleni=? AND id_osetrovatele=?")) {
				$stmt->bind_param('iiii', $id, $id_user, $id, $id_user);
				$result = $stmt->execute();
				if (!$result) {
					$stmt->close();
					//delete
					$query = sprintf("DELETE FROM oddeleni WHERE id_oddeleni = '%s'", $id);
					$db->query($query);
					return false;
				}
				else {
					$stmt->close();
					return true;
				}
			}
			return true;
		}		
	}
}

function checkUnique($name, $type, $db) {
	$query = sprintf("SELECT * FROM oddeleni WHERE nazev = '%s' AND typ_umisteni = '%s'", $db->real_escape_string($name), $db->real_escape_string($type));
	$result = $db->query($query);
	
	if ($result->num_rows >= 1)
		return false;
	else
		return true;
}

if (isset($_GET['edit'])) {
	$edit_id = $_GET['edit'];
	$dep = sprintf("SELECT DISTINCT * FROM oddeleni WHERE id_oddeleni='%s'", $edit_id);
	$depRes = $db->query($dep);
	if (!$depRes) {
		$depRes = "";
	}
	$dp = $depRes->fetch_assoc();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$type = $_POST["type"];
	$name = $_POST["name"];
	if ($type == '' || $name == '') {
		$err = true;
		printError("Required value(s) not set!");
	}
	else {	
		if (checkUnique($name, $type, $db)) {
			if (isset($edit_id)) {
				if ( !editDep($type, $name, $_POST['user'], $db, $edit_id)) {
					$err = true;
					printError("Error: Update failed!");
				}
				else {
					printPass("Update successful!");
				}
			}
			else {
				if ( !postDep($type, $name, $_POST['user'], $db)) {
					$err = true;
					printError("Error: Insert failed!");
				}
				else {
					printPass("Insert successful!");
				}
			}
		}
		else {
			$err = true;
			printError("Error: Department with this name already exists!");
		}
	}
}

if (isset($_GET['edit'])) {
	$edit_id = $_GET['edit'];
	$dep = sprintf("SELECT DISTINCT * FROM oddeleni WHERE id_oddeleni='%s'", $edit_id);
	$depRes = $db->query($dep);
	if (!$depRes) {
		$depRes = "";
	}
	$dp = $depRes->fetch_assoc();
}

$userOpt = sprintf("SELECT id_osetrovatele, login, jmeno, prijmeni FROM osetrovatel ORDER BY prijmeni, jmeno");
$userRes = $db->query($userOpt);
if (!$userRes) {
	die();
}
?>

<br>
<div id="addForm">
	<form <?php if (isset($_GET['edit'])) echo "action='department_add.php?edit=".$edit_id."'"; else echo "action='department_add.php'"; ?> method="post">
		<div class="addFormItem">
			<label> Type: <span class="small">Required</span></label>
			<input type="text" name="type" placeholder="e.g. terarium" <?php if(isset($_POST['type']) && isset($err)) echo ' value="'.$_POST['type'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['typ_umisteni'].'"'; ?>/>
		</div>
		<div class="addFormItem">
			<label> Name: <span class="small">Required (must be unique)</span></label>
			<input type="text" name="name" placeholder="e.g. Terrifying terarium" <?php if(isset($_POST['name']) && isset($err)) echo ' value="'.$_POST['name'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['nazev'].'"'; ?>/>
		</div>
		<div class="addFormItem">
			<label> User: <span class="small">Required</span></label>
			<select name="user">
			<?php
				while ($row = $userRes->fetch_array(MYSQLI_ASSOC)) {
					if ($_POST['user'] == $row['id_osetrovatele'])
						echo "<option value='" . $row['id_osetrovatele'] ."' selected>".$row['jmeno']." ".$row['prijmeni']. " (".$row['login'] .")</option>";
					else
						echo "<option value='" . $row['id_osetrovatele'] ."'>".$row['jmeno']." ".$row['prijmeni']. " (".$row['login'] .")</option>";
				}
			?>
			</select>
		</div>
		<div class="addFormItem">
			<input type="submit" name="submit" <?php if (isset($edit_id)) echo "value='Edit'"; else echo "value='Add'"; ?>>
		</div>
	</form>
</div>

<?php 
if (isset($edit_id))
	echo "<div id='submenu'><div id='deparmenu'><a href='department_list.php'>[<] Back to department list</a></div></div>";

include "footer.php";
?>