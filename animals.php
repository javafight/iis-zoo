<?php
include "session.php";
include "header.php";
require_once "core.php";
?>

<h1>Animals</h1>
<hr>

<div id="submenu">
	<ul>
		<li><a href="animal_list.php">Animal list</a></li>
		
		<?php
		if (hasAuth('edit')) {
			echo "<li><a href='animal_add.php'>Add new animal</a></li>\n";
			echo "<li><a href='animal_species.php'>Add new species</a></li>\n";
		}
		?>

	</ul>
	<hr>
</div>