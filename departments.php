<?php
include "session.php";
include "header.php";
require_once "core.php";
?>

<h1>Departments</h1>
<hr>

<div id="submenu">
	<ul>
		<li><a href="department_list.php">Department list</a></li>
		<?php
		if (hasAuth('edit')) {
			echo "<li><a href='department_add.php'>Add new department</a></li>\n";
			echo "<li><a href='user_department.php'>Assign user to department</a></li>\n";
		}
		?>
	</ul>
	<hr>
</div>