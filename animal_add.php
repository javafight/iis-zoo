<?php
include "animals.php";

 if (!hasAuth('edit')) { //Protect parameters injection
  printUnAuth();
  die();
 }

function postAnimal($id_species, $id_depar, $name, $birth, $db, $litter) {
	if ($id_species == '' || $id_depar == '' || $name == '' || $birth == '')
		return false;
	$insertbirth = date("Y-m-d", strtotime($birth));
	$insertdeath = NULL;

	if ($stmt = $db->prepare("INSERT INTO zivocich (id_druhu, id_oddeleni, jmeno, datum_narozeni, datum_umrti) VALUES (?, ?, ?, ?, ?)")) {
		$stmt->bind_param('iisss', $id_species, $id_depar, $name, $insertbirth, $insertdeath);
		$result = $stmt->execute();
		if (!$result) {
			$stmt->close();
			return false;
		}
		else {
			$stmt->close();

			if ($litter != '') {
				// Get id of inserted record
				$id_animal = $db->insert_id;

				if (!is_numeric($litter) || (int)$litter < 0) {
					//delete
					$query = sprintf("DELETE FROM zivocich WHERE id_zivocicha = '%s'", $id_animal);
					$db->query($query);
					return false;
				}
				if ($stmt = $db->prepare("INSERT INTO samice (id_zivocicha, pocet_vrhu) VALUES (?, ?)")) {
					$stmt->bind_param('ii', $id_animal, $litter);
					$result = $stmt->execute();
					if (!$result) {
						$stmt->close();
						//delete
						$query = sprintf("DELETE FROM zivocich WHERE id_zivocicha = '%s'", $id_animal);
						$db->query($query);
						return false;
					}
					else {
						$stmt->close();
						return true;
					}
				}
			}
			return true;
		}
	}
}

function editAnimal($id_species, $id_depar, $name, $birth, $db, $litter, $id, $death, $poc_vrhu) {
	if ($id_species == '' || $id_depar == '' || $name == '' || $birth == '') {
		printError("Error: Required values not set!");
		return false;
	}
	$insertbirth = date("Y-m-d", strtotime($birth));
	if ($death == NULL)
		$insertdeath = NULL;
	else
		$insertdeath = date("Y-m-d", strtotime($death));

	if ($stmt = $db->prepare("UPDATE zivocich SET id_druhu=?, id_oddeleni=?, jmeno=?, datum_narozeni=?, datum_umrti=? WHERE id_zivocicha=?")) {
		$stmt->bind_param('iisssi', $id_species, $id_depar, $name, $insertbirth, $insertdeath, $id);
		$result = $stmt->execute();
		if (!$result) {
			$stmt->close();
			printError("Error: Update failed!");
			return false;
		}
		else {
			$stmt->close();

			if ($litter != '') {
				if (!is_numeric($litter) || (int)$litter < 0) {
					printError("Error: Litter must be number!");
					return false;
				}
				if ($poc_vrhu == '')
					$stmt = $db->prepare("INSERT INTO samice (id_zivocicha, pocet_vrhu) VALUES (?, ?)");
				else
					$stmt = $db->prepare("UPDATE samice SET id_zivocicha=?, pocet_vrhu=? WHERE id_zivocicha=?");
				if ($stmt) {
					if ($poc_vrhu == '')
						$stmt->bind_param('ii', $id, $litter);
					else
						$stmt->bind_param('iii', $id, $litter, $id);
					$result = $stmt->execute();
					if (!$result) {
						$stmt->close();
						printError("Error: Update failed!");
						return false;
					}
					else {
						$stmt->close();
						return true;
					}
				}
			}
			return true;
		}
	}
}

$deparOpt = sprintf("SELECT DISTINCT oddeleni.id_oddeleni, oddeleni.nazev FROM oddeleni, osetrovatel_oddeleni, osetrovatel ORDER BY oddeleni.nazev ASC", $_SESSION['id']);
$deparRes = $db->query($deparOpt);
if (!$deparRes) {
	$deparRes = "";
}

$specOpt = sprintf("SELECT DISTINCT * FROM druh ORDER BY rod ASC");
$specRes = $db->query($specOpt);
if (!$specRes) {
	$specRes = "";
}

if (isset($_GET['edit'])) {
	$edit_id = $_GET['edit'];
	$flag = true;
	$dep = sprintf("SELECT DISTINCT * FROM zivocich WHERE id_zivocicha='%s'", $edit_id);
	$depRes = $db->query($dep);
	if (!$depRes) {
		$depRes = "";
	}
	$dp = $depRes->fetch_assoc();

	$sam = sprintf("SELECT DISTINCT * FROM samice WHERE id_zivocicha='%s'", $edit_id);
	$samRes = $db->query($sam);
	if (!$samRes) {
		$samRes = "";
	}
	$sm = $samRes->fetch_assoc();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$id_species = $_POST["species"];
	$id_depar = $_POST["department"];
	$name = $_POST["name"];
	$birth = $_POST["birth"];
	if (isset($edit_id))
		$death = $_POST["death"];
	if ($id_species == '' || $id_depar == '' || $name == '' || $birth == '') {
		$err = true;
		printError("Required value(s) not set!");		
	}
	else {
		if (!isset($edit_id)) {
			if ( !postAnimal($id_species, $id_depar, $name, $birth, $db, $_POST["litter"])) {
				$err = true;
				printError("Error: Insert failed!");
			}
			else {
				printPass("Insert succesful!");
			}
		}
		else {
			if ( !editAnimal($id_species, $id_depar, $name, $birth, $db, $_POST["litter"], $edit_id, $death, $sm['pocet_vrhu'])) {
				$err = true;
			}
			else {
				printPass("Update succesful!");
			}
		}

	}
}

if (isset($_GET['edit'])) {
	$edit_id = $_GET['edit'];
	$flag = true;
	$dep = sprintf("SELECT DISTINCT * FROM zivocich WHERE id_zivocicha='%s'", $edit_id);
	$depRes = $db->query($dep);
	if (!$depRes) {
		$depRes = "";
	}
	$dp = $depRes->fetch_assoc();

	$sam = sprintf("SELECT DISTINCT * FROM samice WHERE id_zivocicha='%s'", $edit_id);
	$samRes = $db->query($sam);
	if (!$samRes) {
		$samRes = "";
	}
	$sm = $samRes->fetch_assoc();
}
?>

<br>
<div id="addForm">
	<form <?php if (isset($_GET['edit'])) echo "action='animal_add.php?edit=".$edit_id."'"; else echo "action='animal_add.php'"; ?> method="post">
		<div class="addFormItem">
			<label> Name: <span class="small">Required</span></label>
			<input type="text" name="name" placeholder="e.g. Eduardo" <?php if(isset($_POST['name']) && isset($err)) echo ' value="'.$_POST['name'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['jmeno'].'"';?>/>
		</div>
		<div class="addFormItem">
			<label> Date of birth: <span class="small">Required</span></label>
			<input type="date" name="birth" placeholder='format: YYYY-mm-dd' <?php if(isset($_POST['birth']) && isset($err)) echo ' value="'.$_POST['birth'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['datum_narozeni'].'"';?>/>
		</div>
		<?php if (isset($edit_id)) {
		echo "<div class='addFormItem'>
			<label> Date of death: <span class='small'>Optional</span></label>
			<input type='date' name='death' placeholder='format: YYYY-mm-dd'"; if(isset($_POST['death']) && isset($err)) echo ' value="'.$_POST['death'].'"'; else if (isset($edit_id)) echo ' value="'.$dp['datum_umrti'].'"'; echo '/>
		</div>';}
		?>
		<div class="addFormItem">
			<label> Department: <span class="small">Required</span></label>
			<select name="department">
			<?php
				while ($row = $deparRes->fetch_array(MYSQLI_ASSOC)) {
					if ($_POST['department'] == $row['id_oddeleni'] || (isset($edit_id) && $row['id_oddeleni'] == $dp['id_oddeleni']))
						echo "<option value='" . $row['id_oddeleni'] ."' selected>" . $row['nazev'] ."</option>";
					else	
						echo "<option value='" . $row['id_oddeleni'] ."'>" . $row['nazev'] ."</option>";
				}
			?>
			</select>
		</div>
		<div class="addFormItem">
			<label> Species: <span class="small">Required</span></label>
			<select name="species">
			<?php
				while ($row = $specRes->fetch_array(MYSQLI_ASSOC)) {
					if ($_POST['species'] == $row['id_druhu'] || (isset($edit_id) && $row['id_druhu'] == $dp['id_druhu']))
						echo "<option value='" . $row['id_druhu'] ."' selected>".$row['rod']." ".$row['druh'].", ".$row['celed'].", ".$row['rad'].", ".$row['trida']."</option>";
					else
						echo "<option value='" . $row['id_druhu'] ."'>".$row['rod']." ".$row['druh'].", ".$row['celed'].", ".$row['rad'].", ".$row['trida']."</option>";
				}
			?>
			</select>
		</div>
		<div class="addFormItem">
			<label> Number of litter: <span class="small">Optional (in case of female)</span></label>
			<input type="text" name="litter" placeholder="e.g. 3" <?php if(isset($_POST['litter']) && isset($err)) echo ' value="'.$_POST['litter'].'"'; else if (isset($edit_id)) echo ' value="'.$sm['pocet_vrhu'].'"';?>/>
		</div>
		<div class="addFormItem">
			<input type="submit" name="submit" <?php if (isset($edit_id)) echo "value='Edit'"; else echo "value='Add'"; ?>>
		</div>
	</form>
</div>

<?php
if (isset($edit_id))
	echo "<div id='submenu'><div id='deparmenu'><a href='animal_detail.php?id=".$edit_id."'>[<] Back to animal detail</a><br><a href='animal_list.php'>[<] Back to animal list</a></div></div>";
include "footer.php";
?>