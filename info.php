<?php
include "home.php"
?>

<br>
<h3>Home</h3>
Main page where you can change your password (<b>'Change password'</b>).

<h3>Animals</h3>
You can add new species (<b>'Add new species'</b>) or animals (<b>'Add new animal'</b>) to database here (if you have permission) or browse through the added ones (<b>'Animal list'</b>).

<h3>Departments</h3>
If you'd like to, you can add new department (<b>'Add new department'</b>) to database here or browse through the added ones (<b>'Department list'</b>). If you have permission, you could assign your collegue to department here (<b>'Assign user to department'</b>).

<h3>Users</h3>
You can browse through the added ones (<b>'User list'</b>) here or, if you're administrator, add new user to database (<b>'Add new user'</b>).

<?php
include "footer.php";
?>