<?php
include "departments.php";

//Delete department
if (isset($_GET['del_dep'])) {
	if (!hasAuth('edit')) { //Protect parameters injection
		printUnAuth();
		die();
	}

	//TODO Doladit delete aby slo mazat pouze prazdne oddeleni (ON DELETE restrict)

	$query = sprintf("DELETE FROM oddeleni WHERE id_oddeleni = %s", $db->real_escape_string($_GET['del_dep']));
	
	if ($db->query($query) === TRUE) {
		printPass("Selected Department successfully deleted");
	} else {
		printError("Error deleting department");
	}
}

//Canceling assignments of users on departments
if (isset($_POST['delete']) && isset($_POST['del_id'])) {
	if (!hasAuth('edit')) { //Protect parameters injection
		printUnAuth();
		die();
	}

	$error = FALSE;

	foreach ($_POST['del_id'] as $key => $value) {
		$ids = explode("_", $value);

		$query = "DELETE FROM osetrovatel_oddeleni WHERE id_oddeleni = $ids[0] AND id_osetrovatele = $ids[1]";

		if ($db->query($query) === FALSE)
			$error = TRUE;

	}
	
	if (!$error) {
		printPass("Selected assignment(s) successfully canceled");
	} else {
		printError("Error canceling assignment(s)");
	}

}

//Select all departments
$deps = $db->query("SELECT * FROM oddeleni ORDER BY nazev");

echo "<form action='department_list.php' method='post'>\n";

if ($deps) {
	while ($dep = $deps->fetch_array()) {
		echo "<div class='indent'>\n";
		echo "<h2>$dep[nazev] <i>($dep[typ_umisteni])</i></h2>\n";

		if (hasAuth('edit')) {
			echo "<div class='navig'><a href=department_add.php?edit=$dep[id_oddeleni]>[+] Edit department properties</a></div>\n";
			
			$result = $db->query("SELECT 1 FROM zivocich WHERE id_oddeleni = $dep[id_oddeleni]");
			
			if (!mysqli_num_rows($result)) //Allow deleting department only if no animals are living there (dead or alive)
				echo "<div class='navig'><a href=department_list.php?del_dep=$dep[id_oddeleni]>[-] Delete department</a></div>\n";
		}

		
		/*
		$query = "SELECT id_zivocicha, jmeno, druh, rod, id_oddeleni " .
				"FROM zivocich, druh " .
				"WHERE zivocich.id_druhu = druh.id_druhu AND datum_umrti = 0 AND id_oddeleni = $dep[id_oddeleni] " .
				"ORDER BY jmeno, rod";
		*/
		$query = 	"SELECT os.id_osetrovatele, jmeno, prijmeni, prava, login " .
					"FROM osetrovatel os, osetrovatel_oddeleni od " .
					"WHERE od.id_osetrovatele = os.id_osetrovatele AND od.id_oddeleni = $dep[id_oddeleni] " .
					"ORDER BY prijmeni, jmeno, login";

		$result = $db->query($query);

		if (mysqli_num_rows($result)) {
			echo "<table class='list'>";
			echo "<tr> " . (hasAuth('edit')? "<th>Sel.</th>": "") . "<th>Name</th> <th>Login</th> <th>Permission</th> <th>Det.</th> </tr>\n";

			while ($user = $result->fetch_array()) {
				echo "<tr> " . (hasAuth('edit')? "<td>".delete($dep['id_oddeleni']."_".$user['id_osetrovatele'])."</td>": "") . "<td>$user[jmeno] $user[prijmeni]</td> <td>$user[login]</td>";

				if ($user['prava'] == 1)
					echo "<td>Zookeeper</td>";
				elseif ($user['prava'] == 2)
					echo "<td>Main zookeeper</td>";
				elseif($user['prava'] == 3)
					echo "<td>Administrator</td>";
				else
					echo "<td>???</td>";

				echo "<td>".detail("user_detail.php?id=$user[id_osetrovatele]")."</td> </tr>\n";
			}

			echo "</table>\n";
			echo hasAuth('edit')? "<input type='submit' name='delete' value='Cancel assignment'>\n": "";
		} else {
			echo "No assigned zookeepers";
		}

		echo "</div>\n";
	}
}

echo "</form>\n";

include "footer.php";
?>