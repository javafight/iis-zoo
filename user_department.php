<?php
include "departments.php";

 if (!hasAuth('edit')) { //Protect parameters injection
  printUnAuth();
  die();
 }

function postAssign($id_depar, $id_user, $db) {
	if ($stmt = $db->prepare("INSERT INTO osetrovatel_oddeleni (id_oddeleni, id_osetrovatele) VALUES (?, ?)")) {
		$stmt->bind_param('ii', $id_depar, $id_user);
		$result = $stmt->execute();
		if (!$result) {
			$stmt->close();
			return false;
		}
		else {
			$stmt->close();
			return true;
		}		
	}
}

$userOpt = sprintf("SELECT id_osetrovatele, login, jmeno, prijmeni FROM osetrovatel ORDER BY prijmeni, jmeno");
$userRes = $db->query($userOpt);
if (!$userRes) {
	die();
}

$deparOpt = sprintf("SELECT id_oddeleni, nazev FROM oddeleni ORDER BY nazev");
$deparRes = $db->query($deparOpt);
if (!$deparRes) {
	die();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if ( !postAssign($_POST['department'], $_POST['user'], $db)) {
		printError("Error: Insert failed!");
	}
	else {
		printPass("Insert successful!");	
	}
}
?>

<br>
<div id="addForm">
	<form action="user_department.php" method="post">
		<div class="addFormItem">
			<label> Department: <span class="small">Required</span></label>
			<select name="department">
			<?php
				while ($row = $deparRes->fetch_array(MYSQLI_ASSOC)) {
					if ($_POST['department'] == $row['id_oddeleni'])
						echo "<option value='" . $row['id_oddeleni'] ."' selected>" . $row['nazev'] ."</option>";
					else	
						echo "<option value='" . $row['id_oddeleni'] ."'>" . $row['nazev'] ."</option>";
				}
			?>
			</select>
		</div>
		<div class="addFormItem">
			<label> User: <span class="small">Required</span></label>
			<select name="user">
			<?php
				while ($row = $userRes->fetch_array(MYSQLI_ASSOC)) {
					if ($_POST['user'] == $row['id_osetrovatele'])
						echo "<option value='" . $row['id_osetrovatele'] ."' selected>".$row['jmeno']." ".$row['prijmeni']. " (".$row['login'] .")</option>";
					else	
						echo "<option value='" . $row['id_osetrovatele'] ."'>".$row['jmeno']." ".$row['prijmeni']. " (".$row['login'] .")</option>";
				}
			?>
			</select>
		</div>
		<div class="addFormItem">
			<input type="submit" name="submit" value="Assign">
		</div>
	</form>
</div>



<?php
include "footer.php";
?>