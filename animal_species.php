<?php
include "animals.php";

 if (!hasAuth('edit')) { //Protect parameters injection
  printUnAuth();
  die();
 }

function postSpecies($species, $genus, $family, $order, $class, $db) {
	if ($stmt = $db->prepare("INSERT INTO druh (druh, rod, celed, rad, trida) VALUES (?, ?, ?, ?, ?)")) {
		$stmt->bind_param('sssss', $species, $genus, $family, $order, $class);
		$result = $stmt->execute();
		if (!$result) {
			$stmt->close();
			return false;
		}
		else {
			$stmt->close();
			return true;
		}		
	}
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$species = $_POST["species"];
	$genus = $_POST["genus"];
	$family = $_POST["family"];
	$order = $_POST["order"];
	$class = $_POST["class"];
	if ($species == '' || $genus == '' || $family == '' || $order == '' || $class == '') {
		$err = true;
		printError("Required value(s) not set!");
	}
	else {
		if ( !postSpecies($species, $genus, $family, $order, $class, $db)) {
			$err = true;
			printError("Error: Insert failed!");
		}
		else {
			printPass("Insert successful!");	
		}
	}
}
?>

<br>
<div id="addForm">
	<form action="animal_species.php" method="post">
		<div class="addFormItem">
			<label> Species: <span class="small">Required</span></label>
			<input type="text" name="species" placeholder="e.g. brown" <?php if(isset($_POST['species']) && isset($err)) echo ' value="'.$_POST['species'].'"'; ?>/>
		</div>
		<div class="addFormItem">
			<label> Genus: <span class="small">Required</span></label>
			<input type="text" name="genus" placeholder="e.g. bear" <?php if(isset($_POST['genus']) && isset($err)) echo ' value="'.$_POST['genus'].'"'; ?>/>
		</div>
		<div class="addFormItem">
			<label> Family: <span class="small">Required</span></label>
			<input type="text" name="family" placeholder="e.g. bears" <?php if(isset($_POST['family']) && isset($err)) echo ' value="'.$_POST['family'].'"'; ?>/>
		</div>
		<div class="addFormItem">
			<label> Order: <span class="small">Required</span></label>
			<input type="text" name="order" placeholder="e.g. carnivora" <?php if(isset($_POST['order']) && isset($err)) echo ' value="'.$_POST['order'].'"'; ?>/>
		</div>
		<div class="addFormItem">
			<label> Class: <span class="small">Required</span></label>
			<input type="text" name="class" placeholder="e.g. mammal" <?php if(isset($_POST['class']) && isset($err)) echo ' value="'.$_POST['class'].'"'; ?>/>
		</div>
		<div class="addFormItem">
			<input type="submit" name="submit" value="Add">
		</div>
	</form>
</div>

<?php
include "footer.php";
?>