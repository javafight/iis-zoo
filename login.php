<?php
include "header.php";
require_once "core.php";
?>

<h1>Authentication required</h1>
<hr>

<?php
//Check if user with password exists
function getUser($login, $passwd, $db) {
	//Empty record
	if ($login == '' || $passwd == '')
		return NULL;

	//Construct query
	$query = sprintf("SELECT * from osetrovatel WHERE login='%s'", $db->real_escape_string($login));

	$result = $db->query($query);
	$user = $result->fetch_assoc();

	if (!$user)
		return NULL;

	if (md5($passwd) == $user['heslo']) {
		return $user;
	} else {
		return NULL;
	}
}


if(!empty($_POST)) {
	if (!isset($_SESSION)) {
		session_start();
	}

	if ($user = getUser($_POST['login'], $_POST['password'], $db)) {
		//Login OK
		$_SESSION['activity'] = time();
		$_SESSION['name'] = $user['jmeno'] . " " . $user['prijmeni'];
		$_SESSION['id'] = $user['id_osetrovatele'];
		$_SESSION['right'] = $user['prava'];
		header("Location: info.php");
		die();
	} else {
		//Login Failed
		printError("Invalid login or password");
	}


}

?>


<div id="logForm">
	<form action="login.php" method="post">
		<div class="logFormItem">
			<input type="text" name="login" placeholder="Login" value="<?php echo (isset($_POST['login']))? $_POST['login']: ''?>" />
		</div>
		<div class="logFormItem">
			<input type="password" name="password" placeholder="Password" />
		</div>
		<div class="logFormItem">
			<input type="submit" name="submit" value="LOG IN">
		</div>
	</form>	
</div>

<?php
include "footer.php";
?>